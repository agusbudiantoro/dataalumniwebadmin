import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _2ad2450c = () => interopDefault(import('..\\pages\\Alumni.vue' /* webpackChunkName: "pages/Alumni" */))
const _1ff2e0a8 = () => interopDefault(import('..\\pages\\Alumni_1.vue' /* webpackChunkName: "pages/Alumni_1" */))
const _928a2cd0 = () => interopDefault(import('..\\pages\\AlumniTerbaik.vue' /* webpackChunkName: "pages/AlumniTerbaik" */))
const _2734d126 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))
const _3a8e0e35 = () => interopDefault(import('..\\pages\\diklat.vue' /* webpackChunkName: "pages/diklat" */))
const _39fc7515 = () => interopDefault(import('..\\pages\\google.vue' /* webpackChunkName: "pages/google" */))
const _0581976e = () => interopDefault(import('..\\pages\\icons.vue' /* webpackChunkName: "pages/icons" */))
const _3740aeb1 = () => interopDefault(import('..\\pages\\Jadwal.vue' /* webpackChunkName: "pages/Jadwal" */))
const _1e471786 = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages/login" */))
const _0963c63c = () => interopDefault(import('..\\pages\\notifications.vue' /* webpackChunkName: "pages/notifications" */))
const _96a57172 = () => interopDefault(import('..\\pages\\Pengumuman.vue' /* webpackChunkName: "pages/Pengumuman" */))
const _e3905c8c = () => interopDefault(import('..\\pages\\Pengumuman_2.vue' /* webpackChunkName: "pages/Pengumuman_2" */))
const _9f9ad714 = () => interopDefault(import('..\\pages\\Posting.vue' /* webpackChunkName: "pages/Posting" */))
const _0e97339f = () => interopDefault(import('..\\pages\\Register.vue' /* webpackChunkName: "pages/Register" */))
const _2d02d820 = () => interopDefault(import('..\\pages\\regular.vue' /* webpackChunkName: "pages/regular" */))
const _303d55be = () => interopDefault(import('..\\pages\\rtl.vue' /* webpackChunkName: "pages/rtl" */))
const _4a892ee9 = () => interopDefault(import('..\\pages\\starter-page.vue' /* webpackChunkName: "pages/starter-page" */))
const _30c5c1ea = () => interopDefault(import('..\\pages\\typography.vue' /* webpackChunkName: "pages/typography" */))
const _5c536ab2 = () => interopDefault(import('..\\pages\\user.vue' /* webpackChunkName: "pages/user" */))
const _d25ed5f6 = () => interopDefault(import('..\\pages\\UserList.vue' /* webpackChunkName: "pages/UserList" */))
const _1ad362a0 = () => interopDefault(import('..\\pages\\GeneralViews\\NotFoundPage.vue' /* webpackChunkName: "pages/GeneralViews/NotFoundPage" */))
const _828b50a6 = () => interopDefault(import('..\\pages\\alumni_terbaik\\_id.vue' /* webpackChunkName: "pages/alumni_terbaik/_id" */))
const _3d0c2b02 = () => interopDefault(import('..\\pages\\alumni\\_id_1.vue' /* webpackChunkName: "pages/alumni/_id_1" */))
const _05d75650 = () => interopDefault(import('..\\pages\\alumni\\_id.vue' /* webpackChunkName: "pages/alumni/_id" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'active',
  scrollBehavior,

  routes: [{
    path: "/Alumni",
    component: _2ad2450c,
    name: "Alumni___en"
  }, {
    path: "/Alumni_1",
    component: _1ff2e0a8,
    name: "Alumni_1___en"
  }, {
    path: "/AlumniTerbaik",
    component: _928a2cd0,
    name: "AlumniTerbaik___en"
  }, {
    path: "/ar",
    component: _2734d126,
    name: "index___ar"
  }, {
    path: "/diklat",
    component: _3a8e0e35,
    name: "diklat___en"
  }, {
    path: "/google",
    component: _39fc7515,
    name: "google___en"
  }, {
    path: "/icons",
    component: _0581976e,
    name: "icons___en"
  }, {
    path: "/Jadwal",
    component: _3740aeb1,
    name: "Jadwal___en"
  }, {
    path: "/login",
    component: _1e471786,
    name: "login___en"
  }, {
    path: "/notifications",
    component: _0963c63c,
    name: "notifications___en"
  }, {
    path: "/Pengumuman",
    component: _96a57172,
    name: "Pengumuman___en"
  }, {
    path: "/Pengumuman_2",
    component: _e3905c8c,
    name: "Pengumuman_2___en"
  }, {
    path: "/Posting",
    component: _9f9ad714,
    name: "Posting___en"
  }, {
    path: "/Register",
    component: _0e97339f,
    name: "Register___en"
  }, {
    path: "/regular",
    component: _2d02d820,
    name: "regular___en"
  }, {
    path: "/rtl",
    component: _303d55be,
    name: "rtl___en"
  }, {
    path: "/starter-page",
    component: _4a892ee9,
    name: "starter-page___en"
  }, {
    path: "/typography",
    component: _30c5c1ea,
    name: "typography___en"
  }, {
    path: "/user",
    component: _5c536ab2,
    name: "user___en"
  }, {
    path: "/UserList",
    component: _d25ed5f6,
    name: "UserList___en"
  }, {
    path: "/ar/Alumni",
    component: _2ad2450c,
    name: "Alumni___ar"
  }, {
    path: "/ar/Alumni_1",
    component: _1ff2e0a8,
    name: "Alumni_1___ar"
  }, {
    path: "/ar/AlumniTerbaik",
    component: _928a2cd0,
    name: "AlumniTerbaik___ar"
  }, {
    path: "/ar/diklat",
    component: _3a8e0e35,
    name: "diklat___ar"
  }, {
    path: "/ar/google",
    component: _39fc7515,
    name: "google___ar"
  }, {
    path: "/ar/icons",
    component: _0581976e,
    name: "icons___ar"
  }, {
    path: "/ar/Jadwal",
    component: _3740aeb1,
    name: "Jadwal___ar"
  }, {
    path: "/ar/login",
    component: _1e471786,
    name: "login___ar"
  }, {
    path: "/ar/notifications",
    component: _0963c63c,
    name: "notifications___ar"
  }, {
    path: "/ar/Pengumuman",
    component: _96a57172,
    name: "Pengumuman___ar"
  }, {
    path: "/ar/Pengumuman_2",
    component: _e3905c8c,
    name: "Pengumuman_2___ar"
  }, {
    path: "/ar/Posting",
    component: _9f9ad714,
    name: "Posting___ar"
  }, {
    path: "/ar/Register",
    component: _0e97339f,
    name: "Register___ar"
  }, {
    path: "/ar/regular",
    component: _2d02d820,
    name: "regular___ar"
  }, {
    path: "/ar/rtl",
    component: _303d55be,
    name: "rtl___ar"
  }, {
    path: "/ar/starter-page",
    component: _4a892ee9,
    name: "starter-page___ar"
  }, {
    path: "/ar/typography",
    component: _30c5c1ea,
    name: "typography___ar"
  }, {
    path: "/ar/user",
    component: _5c536ab2,
    name: "user___ar"
  }, {
    path: "/ar/UserList",
    component: _d25ed5f6,
    name: "UserList___ar"
  }, {
    path: "/GeneralViews/NotFoundPage",
    component: _1ad362a0,
    name: "GeneralViews-NotFoundPage___en"
  }, {
    path: "/ar/GeneralViews/NotFoundPage",
    component: _1ad362a0,
    name: "GeneralViews-NotFoundPage___ar"
  }, {
    path: "/ar/alumni_terbaik/:id?",
    component: _828b50a6,
    name: "alumni_terbaik-id___ar"
  }, {
    path: "/ar/alumni/:id_1?",
    component: _3d0c2b02,
    name: "alumni-id_1___ar"
  }, {
    path: "/ar/alumni/:id?",
    component: _05d75650,
    name: "alumni-id___ar"
  }, {
    path: "/alumni_terbaik/:id?",
    component: _828b50a6,
    name: "alumni_terbaik-id___en"
  }, {
    path: "/alumni/:id_1?",
    component: _3d0c2b02,
    name: "alumni-id_1___en"
  }, {
    path: "/alumni/:id?",
    component: _05d75650,
    name: "alumni-id___en"
  }, {
    path: "/",
    component: _2734d126,
    name: "index___en"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}

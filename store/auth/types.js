export const Actionauth = {
    RefreshToken:"RefreshToken",
    Login : "Login",
    Login1 : 'Login1',
    Logout: "Logout",
    parseJWT:'parseJWT',
    // api
    diklat:'diklat',
    cekuser:'cekuser'
};

export const Mutationauth ={
    RefreshToken:"RefreshToken",
    Login : "Login",
    request: "auth_request",
    success: "auth_success",
    error : "auth_error",
    Logout : "Logout",
    Islogin : "Islogin",
    Role:"Role",
    Jenisteknis:"Jenisteknis"

};

export const Getterauth = {
    getRole:"getRole",
}



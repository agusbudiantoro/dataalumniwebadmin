import {Actionauth,Mutationauth} from '../types'
import Axios from 'axios'
import qs from 'qs';
import { thresholdScott } from 'd3';
import { startOfWeek } from 'date-fns';


export default {
    [Actionauth.RefreshToken]({commit,state}){
        return new Promise ((resolve,reject)=>{
            console.log("sini");
            var cred = btoa('USER_CLIENT_APP'+':'+'password');
            //commit('auth_request')
            // Axios.post('http://localhost:9001/oauth/token',null,
            Axios.post('https://smartbangkom.lan.go.id:9001/oauth/token',null,
            {
                params:{
                    "grant_type":"refresh_token",
                    "refresh_token":window.$nuxt.$cookies.get('refreshToken'),
                },
                auth: {
                    username: 'USER_CLIENT_APP',
                    password: 'password',
                },
                
            }
            )
            .then(res=>{
                const refreshToken = res.data.refresh_token
                const token = res.data.access_token
                const user = res.data.user
                const lemdik = res.data.lemdik
                const idUser = res.data.iduser
                const akronim = res.data.akronim
                window.$nuxt.$cookies.set('token',token)
                window.$nuxt.$cookies.set('refreshToken',refreshToken)
                window.$nuxt.$cookies.set('namaLemdik',lemdik)
                window.$nuxt.$cookies.set('idUser',idUser)
                window.$nuxt.$cookies.set('akronim',akronim)
                var base64Url = token.split('.')[1]
                var base64 = base64Url.replace(/-/g,'+').replace(/_/g,'/')
                var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c){
                    return '%' + ('00'+ c.charCodeAt(0).toString(16)).slice(-2)
                }).join(''))

                

                console.log(res)
                window.$nuxt.$cookies.set('isijwt',jsonPayload)
                Axios.defaults.headers.common['Authorization']='Basic '+token
                var parsejwt = JSON.parse(jsonPayload)
                console.log(parsejwt.authorities[0])
                // if(parsejwt.authorities[0]=="role_peserta"){
                //     this.$router.push('/dashboard/register')
                    
                // }else{
                    this.$router.push('/')
                // }
                // let routeData = this.$router.resolve({name: '/dashboard'});
                //     window.open(routeData.href, '_blank');

            })
            .catch(err=> {
                err=> console.warn(err)
                localStorage.removeItem('token')
                reject(err)
            })
        })
    },
     [Actionauth.Login]({commit,state},user){
        return new Promise ((resolve,reject)=>{
            var cred = btoa('USER_CLIENT_APP'+':'+'password');
            //commit('auth_request')
            // Axios.post('http://localhost:9001/oauth/token',null,
            Axios.post('https://smartbangkom.lan.go.id:9001/oauth/token', null,
            {
                params:{
                    "grant_type":"password",
                    "username":user.email,
                    "password":user.password
                },
                auth: {
                    username: 'USER_CLIENT_APP',
                    password: 'password',
                },
                
            }
            )
            .then(res=>{
                const refreshToken = res.data.refresh_token
                const token = res.data.access_token
                const user = res.data.user
                const lemdik = res.data.lemdik
                const idUser = res.data.iduser
                const akronim = res.data.akronim
                window.$nuxt.$cookies.set('token',token)
                window.$nuxt.$cookies.set('refreshToken',refreshToken)
                window.$nuxt.$cookies.set('namaLemdik',lemdik)
                window.$nuxt.$cookies.set('idUser',idUser)
                window.$nuxt.$cookies.set('akronim',akronim)
                var base64Url = token.split('.')[1]
                var base64 = base64Url.replace(/-/g,'+').replace(/_/g,'/')
                var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c){
                    return '%' + ('00'+ c.charCodeAt(0).toString(16)).slice(-2)
                }).join(''))

                

                console.log(res)
                window.$nuxt.$cookies.set('isijwt',jsonPayload)
                Axios.defaults.headers.common['Authorization']='Basic '+token
                var parsejwt = JSON.parse(jsonPayload)
                console.log(parsejwt.authorities[0])
                // if(parsejwt.authorities[0]=="role_peserta"){
                //     this.$router.push('/dashboard/register')
                    
                // }else{
                    this.$router.push('/')
                // }
                // let routeData = this.$router.resolve({name: '/dashboard'});
                //     window.open(routeData.href, '_blank');

            })
            .catch(err=> {
                err=> console.warn(err)
                localStorage.removeItem('token')
                reject(err)
            })
        })
    },

    [Actionauth.cekuser](){
        return new Promise((resolve,reject)=>{
            Axios.get('https://smartbangkom.lan.go.id:8094/master/cekuser',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Actionauth.parseJWT](token){
        console.log('isi :',token)
        

        //return JSON.parse(jsonPayload)
        return token
    },

    [Actionauth.Logout]({commit,context}){
        window.$nuxt.$cookies.removeAll()
        commit(`${[Mutationauth.Islogin]}`,false)
        
    }

}
import {Action,Mutation} from '../types'
import Axios from 'axios'
import qs from 'qs';
import { thresholdScott } from 'd3';
import { startOfWeek } from 'date-fns';


import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

var urldiklat="https://smartbangkom.lan.go.id:8091/";
var urlpeserta="https://smartbangkom.lan.go.id:8092/";
var urlperizinan="https://smartbangkom.lan.go.id:8093/";
var urluser ="https://smartbangkom.lan.go.id:8090/";
var urlmaster="https://sipka.lan.go.id/api/";
var urlDev ="https://smartbangkom.lan.go.id:8094/";

let url_jadwal = 'https://dev-smartbangkom.lan.go.id:9093/Jadwal';
let url_pengumuman = 'https://dev-smartbangkom.lan.go.id:9093/Pengumuman';
let url_posting = 'https://dev-smartbangkom.lan.go.id:9091/posting';
let url_alumni = 'https://dev-smartbangkom.lan.go.id:9092/alumni';
let url_alumni_terbaik = 'https://dev-smartbangkom.lan.go.id:9093/alumniTerbaik';
// let test_url_alumni_terbaik = 'https://dev-smartbangkom.lan.go.id:9093/alumniTerbaik',
let url_pendidikan = 'https://dev-smartbangkom.lan.go.id:9092/pendidikan';
let url_organisasi = 'https://dev-smartbangkom.lan.go.id:9092/organisasi';
let url_proper = 'https://dev-smartbangkom.lan.go.id:9092/proper';
let url_master_diklat = 'https://dev-smartbangkom.lan.go.id:9093/masterDiklat';
let url_register = 'https://dev-smartbangkom.lan.go.id:9093/Register';

export default {
    [Action.alumni.peserta]({}, page){
        return new Promise((resolve,reject) => {
            Axios.get(url_alumni+'/'+page+'/10', { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },
    [Action.alumni.editAlumni]({}, data){
        return new Promise((resolve,reject) => {
            const isi={
                "agama": data.agama,
                "alamatKantor": data.alamatKantor,
                "asalInstansi": data.asalInstansi,
                "email": data.email,
                "gol": data.gol,
                "jabatan": data.jabatan,
                "jenisKelamin": data.jenisKelamin,
                "nama": data.nama,
                "noTelp":data.noTelp,
                "nomorId": data.nomorId,
                "nomorKra": data.nomorKra,
                "pangkat": data.pangkat,
                "tempatLahir": data.tempatLahir,
            }
            Axios.put(url_alumni+"/"+data.id, data, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res);
                console.log(res);
            })
            .catch(err => {
                reject(err)
            })
        })
    },
    [Action.alumni.peserta_by_id]({}, value){
        return new Promise((resolve,reject) => {
            Axios.get(url_alumni+'/'+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.carialumni]({}, data){
        return new Promise((resolve,reject) => {
            Axios.post(url_alumni+'/caribynama/'+data.page+"/10?nama="+data.nama,'', { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.peserta_test]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_alumni, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_photo]({}, value){
        return new Promise((resolve,reject) => {
            Axios.post(url_alumni+'/foto', value.formKata, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'), 'Content-Type': 'multipart/form-data' }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.ambil_photo]({}, value){
        return new Promise((resolve,reject) => {
            Axios.post(url_alumni+'/gambarori/'+value.name, '', { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    // [Action.alumni.peserta_by_id]({}, value){
    //     return new Promise((resolve,reject) => {
    //         Axios.get(url_alumni+'/'+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
    //         .then(res => {
    //             resolve(res)
    //         })
    //         .catch(err => {
    //             reject(err)
    //         })
    //     })
    // },

    [Action.alumni.peserta_terbaik]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_alumni_terbaik, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_peserta_terbaik]({}, value){
        return new Promise((resolve,reject) => {
            // let data_alumni_terbaik = {"idAlumni": value.formKata.idAlumni, "iddiklat": value.formKata.iddiklat, "urutan": value.formKata.urutan};
            Axios.post(url_alumni_terbaik, value.formKata, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.peserta_terbaik_by_diklat]({}, value){
        return new Promise((resolve,reject) => {
            Axios.get(url_alumni_terbaik+'/bydiklat/'+value.id, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.peserta_terbaik_get_all]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_alumni_terbaik+'/getallbydiklat', { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_peserta_terbaik]({}, value){
        return new Promise((resolve,reject) => {
            Axios.delete(url_alumni_terbaik+'/'+value.id, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.jadwal]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_jadwal, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_jadwal]({}, value){
        return new Promise((resolve,reject) => {
            // console.log(formKata.formKata);
            
            Axios.post(url_jadwal, value.formKata, {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
            .then(res => {
                resolve(res)
                // return true;
            })
            .catch(err => {
                reject(err)
            })
        })
    },
    
    [Action.alumni.update_jadwal]({}, value){
        return new Promise((resolve,reject) => {
            // console.log(formKata.formKata);
            
            Axios.put(url_jadwal+'/'+value.id, value.formKata, {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
            .then(res => {
                resolve(res)
                // return true;
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_jadwal]({}, value){
        return new Promise((resolve,reject) => {
            // console.log(formKata.formKata);
            
            Axios.delete(url_jadwal+'/'+value.id, {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
            .then(res => {
                resolve(res)
                // return true;
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.pengumuman]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_pengumuman, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_pengumuman]({}, value){
        return new Promise((resolve,reject) => {
            // console.log(value);
            
            // let data = {"judul":value.deskripsi,"deskripsi":value.deskripsi,"tglpelaksanaan":new Date()};
            // let data = {"judul":value.deskripsi,"deskripsi":value.deskripsi,"tglpelaksanaan":new Date(),"gambar":value.gambar};
            // console.log(data);
            // let fileSurat = this.$refs.fileSurat.files[0]
            // let formKata = new FormData()
            //     formKata.append("gambar", value.gambar)
            //     formKata.append("pengumuman", JSON.stringify(data))
            
            Axios.post(url_pengumuman, value,{headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'), 'Content-Type': 'multipart/form-data'}})
            .then(res => {
                resolve(res)
                // return true;
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.update_pengumuman]({}, value){
        return new Promise((resolve,reject) => {
            // console.log(value);
            
            // let data = {"judul":value.deskripsi,"deskripsi":value.deskripsi,"tglpelaksanaan":new Date()};
            // let data = {"judul":value.deskripsi,"deskripsi":value.deskripsi,"tglpelaksanaan":new Date(),"gambar":value.gambar};
            // console.log(data);
            // let fileSurat = this.$refs.fileSurat.files[0]
            // let formKata = new FormData()
            //     formKata.append("gambar", value.gambar)
            //     formKata.append("pengumuman", JSON.stringify(data))
            
            Axios.post(url_pengumuman+'/'+value.id, value.formKata, {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'), 'Content-Type': 'multipart/form-data'}})
            .then(res => {
                resolve(res)
                // return true;
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_pengumuman]({},value){
        return new Promise((resolve,reject) => {
            // console.log(value);
            
            Axios.delete(url_pengumuman+'/'+value.id,{headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
            .then(res => {
                resolve(res)
                // return true;
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.gambar_pengumuman]({}, value){
        return new Promise((resolve,reject) => {
            // console.log('val '+value);
            for (let i = 0; i < value.length; i++) {
                const element = value[i];
                if (element.value != null) {
                    Axios.post(url_pengumuman+'/gambarori/'+element.value, "", {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
                    .then(res => {
                        // return res.data;
                        resolve(res)
                    })
                    .catch(err => {
                        reject(err)
                    })
                }
            }
            // let data = {"judul":value.deskripsi,"deskripsi":value.deskripsi,"tglpelaksanaan":new Date()};
            // let data = {"judul":value.deskripsi,"deskripsi":value.deskripsi,"tglpelaksanaan":new Date(),"gambar":value.gambar};
            // console.log(data);
            // let fileSurat = this.$refs.fileSurat.files[0]
            // let formKata = new FormData()
            //     formKata.append("gambar", value.gambar)
            //     formKata.append("pengumuman", JSON.stringify(data))
        })
    },

    [Action.alumni.test_gambar_pengumuman]({}, value){
        return new Promise((resolve,reject) => {
            console.log('store : '+value);
            
            // Axios.post(url_pengumuman+'/gambarori/'+value.value, "", {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
            // .then(res => {
            //     // return res.data;
            //     resolve(res)
            // })
            // .catch(err => {
            //     reject(err)
            // })
        })
    },
    
    [Action.alumni.posting]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_posting, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },
    
    [Action.alumni.simpan_posting]({}, value){
        return new Promise((resolve,reject) => {
            Axios.post(url_posting, value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.update_posting]({}, value){
        return new Promise((resolve,reject) => {

            // let data = { 'judul': value.judul, 'caption': value.caption, 'gambar': value.gambar, 'url': value.url };

            Axios.post(url_posting+'/'+value.id, value.formKata, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'), 'Content-Type': 'multipart/form-data'}})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_posting]({}, value){
        return new Promise((resolve,reject) => {
            Axios.delete(url_posting+'/'+value.id, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.test_gambar_posting]({}, value){
        return new Promise((resolve,reject) => {
            Axios.post(url_posting+'/gambarori/'+value.gambar, "", {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.pendidikan_test]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_pendidikan, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.pendidikan]({}, value){
        return new Promise((resolve,reject) => {
            Axios.get(url_pendidikan+"/"+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_pendidikan]({}, value){
        return new Promise((resolve,reject) => {
            // console.log(value.formKata);
            let pendidikan = {"jenjang": value.formKata.jenjang, "namauniv": value.formKata.namauniv, "lokasi": value.formKata.lokasi, "tahun": value.formKata.tahun, "idAlumni": value.formKata.idAlumni};
            // console.log(pendidikan);
            
            Axios.post(url_pendidikan, pendidikan, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.update_pendidikan]({}, value){
        return new Promise((resolve,reject) => {
            // console.log(value.formKata);
            let pendidikan = {"jenjang": value.formKata.jenjang, "namauniv": value.formKata.namauniv, "lokasi": value.formKata.lokasi, "tahun": value.formKata.tahun, "idAlumni": value.formKata.idAlumni};
            // console.log(pendidikan);
            
            Axios.put(url_pendidikan+"/"+value.formKata.id, pendidikan, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_pendidikan]({}, value){
        return new Promise((resolve,reject) => {            
            Axios.delete(url_pendidikan+"/"+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.organisasi]({}, value){
        return new Promise((resolve,reject) => {
            Axios.get(url_organisasi+"/"+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_organisasi]({}, value){
        return new Promise((resolve,reject) => {
            const organisasi = {"bidangOrganisasi": value.formKata.bidangOrganisasi, "jabatan": value.formKata.jabatan, "namaOrganisasi": value.formKata.namaOrganisasi, "tahun": value.formKata.tahun, "idAlumni": value.formKata.idAlumni}
            Axios.post(url_organisasi, organisasi, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.update_organisasi]({}, value){
        return new Promise((resolve,reject) => {
            const organisasi = {"bidangOrganisasi": value.formKata.bidangOrganisasi, "jabatan": value.formKata.jabatan, "namaOrganisasi": value.formKata.namaOrganisasi, "tahun": value.formKata.tahun, "idAlumni": value.formKata.idAlumni, "id": value.formKata.id}
            Axios.put(url_organisasi+"/"+value.formKata.id, organisasi, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_organisasi]({}, value){
        return new Promise((resolve,reject) => {
            Axios.delete(url_organisasi+"/"+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.proper_test]({}, value){
        return new Promise((resolve,reject) => {
            Axios.get(url_proper, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.proper]({}, value){
        return new Promise((resolve,reject) => {
            Axios.get(url_proper+'/'+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_proper]({}, value){
        return new Promise((resolve,reject) => {
            const proper = {"idAlumni": value.formKata.idAlumni, "judul": value.formKata.judul, "abstrak": value.formKata.abstrak, "evidence": value.formKata.evidence, "kataKunci": value.formKata.kataKunci, "tahun": value.formKata.tahun}
            Axios.post(url_proper, proper, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.update_proper]({}, value){
        return new Promise((resolve,reject) => {
            const proper = {"idAlumni": value.formKata.idAlumni, "judul": value.formKata.judul, "abstrak": value.formKata.abstrak, "evidence": value.formKata.evidence, "kataKunci": value.formKata.kataKunci, "tahun": value.formKata.tahun};
            Axios.put(url_proper+'/'+value.formKata.id, proper, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_proper]({}, value){
        return new Promise((resolve,reject) => {
            Axios.delete(url_proper+"/"+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.detail_proper]({}, value){
        return new Promise((resolve,reject) => {
            Axios.get(url_proper+"/"+value, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.master_diklat]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_master_diklat, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_master_diklat]({}, value){
        return new Promise((resolve,reject) => {
            let data_master_diklat = {"juduldiklat": value.formKata.juduldiklat, "tahun": value.formKata.tahun};
            Axios.post(url_master_diklat, data_master_diklat, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.update_master_diklat]({}, value){
        return new Promise((resolve,reject) => {
            let data_master_diklat = {"juduldiklat": value.formKata.juduldiklat, "tahun": value.formKata.tahun};
            Axios.put(url_master_diklat+'/'+value.id, data_master_diklat, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_master_diklat]({}, value){
        return new Promise((resolve,reject) => {
            // let data_master_diklat = {"juduldiklat": value.formKata.juduldiklat, "tahun": value.formKata.tahun};
            Axios.delete(url_master_diklat+'/'+value.id, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.register]({}){
        return new Promise((resolve,reject) => {
            Axios.get(url_register, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.simpan_register]({}, value){
        return new Promise((resolve,reject) => {
            let data_register = {"nip": value.formKata.nip, "nama": value.formKata.nama, "password": value.formKata.password, "verifikasi": value.formKata.verifikasi};
            Axios.post(url_register, data_register, {headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')}})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.update_register]({}, value){
        return new Promise((resolve,reject) => {
            let data_register = {"nip": value.formKata.nip, "nama": value.formKata.nama, "password": value.formKata.password, "verifikasi": value.formKata.verifikasi};
            Axios.put(url_register+'/'+value.id, data_register, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.hapus_register]({}, value){
        return new Promise((resolve,reject) => {
            // let data_master_diklat = {"juduldiklat": value.formKata.juduldiklat, "tahun": value.formKata.tahun};
            Axios.delete(url_register+'/'+value.id, { headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    [Action.alumni.validasi_register]({}, value){
        return new Promise((resolve,reject) => {
            // let data_master_diklat = {"juduldiklat": value.formKata.juduldiklat, "tahun": value.formKata.tahun};
            Axios.post(url_register+'/'+value.id, '',{ headers:{ 'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token') }})
            .then(res => {
                resolve(res)
            })
            .catch(err => {
                reject(err)
            })
        })
    },

    //api diklat
    //upload
    [Action.diklat.uploadkra]({},isi){
    
        var formdata = new FormData()
        formdata.append('permintaankra',isi.permintaan)
        formdata.append('daftarhadir',isi.daftarhadir)

        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/uploaddokumen/'+isi.iddiklat,formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    
    //end upload
    [Action.diklat.downloaddoc]({},nama){
    
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/downloaddockra/'+nama+'.',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    
    [Action.diklat.penandatangan]({},isi){
        return new Promise((resolve,reject)=>{
           
            Axios.put(urldiklat+'diklat/penandatangan/'+isi.iddiklat+"/"+isi.ttd1+"/"+isi.ttd2+"/"+isi.ttd3,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambilsemuadiklat](){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildiklatbyuser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/byuser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildiklatbyuserid]({},iduser){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/byiduser/'+iduser,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.simpandiklat]({},isi){
        //isi data {isidiklat:{},detail:"",angkatan:""
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('isidiklat',JSON.stringify(isi.isidiklat))
            formdata.append('detail',detail)
            formdata.append('angkatan',angkatan)

            Axios.post(urldiklat+'diklat/simpan',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.kirimdiklat]({},isi){
        //isi data {isidiklat:{},detail:"",angkatan:""
        return new Promise((resolve,reject)=>{
            var formdata = new FormData()
            formdata.append('isidiklat',JSON.stringify(isi.isidiklat))
            formdata.append('detail',detail)
            formdata.append('angkatan',angkatan)

            Axios.post(urldiklat+'diklat/kirim',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.diklat.hitungpeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/countpeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambildetaildiklat]({},id){
        console.log(id);
        
        return new Promise((resolve,reject)=>{
            Axios.get(urldiklat+'diklat/detail/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res); console.log('sini');}
            ).catch(err=>{reject(err); console.log('sono');}
            )
        })
    },
    [Action.diklat.simpandetail]({},isi){
        //data isi {jumpes:0,asal:"",sumber:"",pola:""}
        return new Promise((resolve,reject)=>{
            let data={
                "jumpes":isi.jumpes,
                "asalInstansi":isi.asal,
                "sumberAnggaran":isi.sumber,
                "polaPenyelenggaraan":isi.pola
            };
            Axios.post(urldiklat+'diklat/detail/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ubahtanggalttd]({},isi){
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            Axios.put(urldiklat+'diklat/tglttd/'+isi.iddiklat+'/'+isi.tgl+'/'+isi.tipe,'', {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
        }) 
    },
    [Action.diklat.editdetail]({},isi){
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            for (let n = 0; n < isi.length; n++) {
                const element_up = isi[n];
                Axios.put(urldiklat+'diklat/editDetail/'+element_up.id, element_up, {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
            }
        }) 
    },
    [Action.diklat.deletedetail]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/detail/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.deletediklat]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.ambilangkatan]({},id){
        console.log(id);
        
        return new Promise((resolve,reject)=>{
            Axios.get('https://smartbangkom.lan.go.id:8091/diklat/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{
                resolve(res)
                console.log(res);
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.simpanangkatan]({},isi){
        // data isi {alamat:"",daerah:""}
        return new Promise((resolve,reject)=>{
            let data={
                "alamat":isi.alamat,
	            "daerah":isi.daerah
            };
            Axios.post(urldiklat+'diklat/angkatan/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.editangkatan]({},isi){
        // data isi {alamat:"",daerah:""}
        return new Promise((resolve,reject)=>{
            let headers = { "Authorization":'Bearer '+window.$nuxt.$cookies.get('token') }
            for (let n = 0; n < isi.length; n++) {
                const element_up_angkatan = isi[n];
                Axios.put(urldiklat+'diklat/angkatan/'+element_up_angkatan.id, element_up_angkatan, {headers:headers})
                .then(res => {
                    resolve(res)
                })
                .catch(err => {
                    reject(err)
                })
            }
        })
    },
    [Action.diklat.delelteangkatan]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urldiklat+'diklat/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.diklat.postTemplateTeknis]({},isi){
        var formdata = new FormData()
        formdata.append('formatcert',isi.file[0])
        return new Promise((resolve, reject)=>{
            Axios.put(urldiklat+"diklat/uploadformat/"+isi.idDiklat,formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.diklat.putValidasi]({},isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/validasiatasan/'+isi.id+'/'+isi.value,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end diklat

    //api kra
    [Action.cert.datagbr]({},namagbr){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/gambarttd/'+namagbr+'.','',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.cert.gbtttd]({},isi){
        var formdata = new FormData()
        formdata.append('gambar',isi.base64)
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/simpangbrttd/'+isi.iduser,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.cert.cekFile]({},isi){
        var formdata = new FormData()
        formdata.append('dokumen', isi)
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/cekfile/',formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.cert.ttdcert]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttddigital/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(function (error) {
                reject(error)
                if (error.response) {
                // commit(`${[Mutation.error]}`,error.response.status)
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
                }
            }
            
            )
        })  
    },
    [Action.cert.ttdcertkalan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttdkalandev/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.cert.ttdcertdeputi]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttddeputidev/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },

    //riil
    [Action.cert.cekganti]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ubahpdf/'+isi.idpeserta,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.cert.tandacert]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttdlemdik/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.cert.tandakalan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttdkalan/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.cert.tandadeputi]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/ttddeputi/'+isi.id+"/"+isi.idpeserta+"/"+isi.iduser+"/"+isi.pass,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    //endriil
    [Action.cert.generate]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'digitalsignature/gencert/'+isi.id,{
                "jp":isi.jp,
                "penyelenggara":isi.p,
                "status":isi.sts,
                "pm":isi.pm,
                "pminstansi":isi.pminstansi,
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })  
    },
    [Action.kra.ambil](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'kra',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.kra.edit]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'kra/'+isi.id,{
                "jenisprogram":isi.jenis,
                "nomor":isi.nomor,
                "tahun":isi.tahun
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api kra

    //api peserta
    [Action.peserta.tambahkra]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/tambahkra/'+isi.idpeserta+'/'+isi.idkra,{
                "nomorKra":isi.kra
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.tambahnilai]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlpeserta+'peserta/tambahnilai/'+isi.idpeserta,{
                "proyekPerubahan":isi.proyek,
                "nilai":isi.nilai
            },{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.peserta.updatePeserta]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/updatekuota/'+isi.iddiklat+'/'+isi.kuota,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyid]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyrole]({},role){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftarbyrole/'+role,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilbkn]({},nip){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'bkn/datapegawai/'+nip,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.simpanpesertakelompok]({},isi){
        //data isi {instansi:"",alamat:"",dokumen:"",jeniskerja:"",id:0}
        let form={
            "asalInstansi":isi.instansi,
            "alamatKantor":isi.alamat,
            "jenisPekerjaan":isi.jeniskerja,
        }
        var formdata = new FormData()
        formdata.append('excel',isi.dokumen)
        formdata.append('isi',JSON.stringify(form))

        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/kelompok/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabydiklat]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyIdPeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/userPeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertatanpaid]({}){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.revisiSttp]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/revisiSttp/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.ambilpesertabyangkatan]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/angkatan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.simpanpeserta]({},isi){
        //data isi {id:1,jeniskerja:"",nomorid:"",nama:"",ttl:"",jenkel:"",agama:"",pangkat:"",email:"",asalinstansi:"",alamatkantor:"",notelp:"",sumber:"",pola:"",iddiklat:"",gol:""}
        return new Promise((resolve,reject)=>{
            let data={
                    "jabatan":isi.jabatan,
                    "jenis": isi.jenis,
                    "jenisPekerjaan": isi.jeniskerja,
                    "nomorId": isi.nomorid,
                    "nama": isi.nama,
                    "tempatLahir": isi.ttl,
                    "jenisKelamin": isi.jenkel,
                    "agama": isi.agama,
                    "pangkat": isi.pangkat,
                    "email": isi.email,
                    "asalInstansi":isi.asalinstansi,
                    "alamatKantor": isi.alamatkantor,
                    "noTelp": isi.notelp,
                    "sumberAnggaran": isi.sumber,
                    "polaPenyelenggaraan": isi.pola,
                    "idDiklat": isi.iddiklat,
                    "gol": isi.gol
            };
            Axios.post(urlpeserta+'peserta',data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        }) 
    },
    [Action.peserta.editpeserta]({},isi){
        //data isi {id:1,jeniskerja:"",nomorid:"",nama:"",ttl:"",jenkel:"",agama:"",pangkat:"",email:"",asalinstansi:"",alamatkantor:"",notelp:"",sumber:"",pola:"",iddiklat:"",gol:""}
        return new Promise((resolve,reject)=>{
            console.log(isi);
            
            let data={
                    "jenis": isi.jenis,
                    "jenisPekerjaan": isi.jeniskerja,
                    "nomorId": isi.nomorid,
                    "nama": isi.nama,
                    "tempatLahir": isi.ttl,
                    "jenisKelamin": isi.jenkel,
                    "agama": isi.agama,
                    "pangkat": isi.pangkat,
                    "email": isi.email,
                    "asalInstansi":isi.asalinstansi,
                    "alamatKantor": isi.alamatkantor,
                    "noTelp": isi.notelp,
                    "sumberAnggaran": isi.sumber,
                    "polaPenyelenggaraan": isi.pola,
                    "idDiklat": isi.iddiklat,
                    "gol": isi.gol
            };
            Axios.put(urlpeserta+'peserta/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        }) 
    },
    [Action.peserta.editfotopeserta]({},isi){
        //isi data {id:0,foto:filefoto}
        console.log(isi);
        return new Promise((resolve,reject)=>{
            let formdata = new FormData()
		    formdata.append('foto', isi.foto)
            Axios.put(urlpeserta+'peserta/editfoto/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.getfotopeserta]({},foto){
        //isi data {id:0,foto:filefoto}
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/getfoto/'+foto+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.deletepeserta]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urlpeserta+'peserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.pindahangkatan]({},isi){
        //data isi {id:0,angkatan:0}
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/kelaspeserta/'+isi.id+"/"+isi.angkatan,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.pindahdiklat]({},isi){
        //data isi {id:0,angkatan:0}
        return new Promise((resolve,reject)=>{
            Axios.post(urlpeserta+'peserta/pindahpeserta/'+isi.id+"/"+isi.angkatan,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.peserta.downloadsampel]({},nama){
        return new Promise((resolve,reject)=>{
            Axios.get(urlpeserta+'peserta/download/'+nama,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api peserta

    //api izin
    [Action.izin.ambilizin](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.ambilizinbyuser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/izinbyuser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.editizin]({},isi){
        //data isi {id:0,iddiklat:0,penjaminmutu:"",izindeputi:""}
        return new Promise((resolve,reject)=>{
            let data={
                "idDiklat": isi.iddiklat,
                "penjaminMutu": isi.penjaminmutu,
                "izinDeputi": isi.izindeputi,
            }
            Axios.put(urlperizinan+'perizinan/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.deleteizin]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urlperizinan+'perizinan/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.ajukanizin]({},isi){
        console.log(isi.penjaminmutu);
        
        //data isi {id:0,penjaminmutu:"",dokumen:filedokumen}
        let data ={
            "penjaminMutu":isi.penjaminmutu,
        }
        var formdata = new FormData()
        formdata.append('dokumen',isi.dokumen)
        formdata.append('isi',JSON.stringify(data))

        return new Promise((resolve,reject)=>{
            Axios.post(urlperizinan+'perizinan/ajukanizin/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.verifikasiizin]({},isi){
        //data isi {id:0,penjaminmutu:"",izindeputi:"",dokumen:filedokumen}
        console.log(isi);
        
        let data ={
            "penjaminMutu":isi.penjaminMutu,
            "izinDeputi": isi.izinDeputi,
        }
        var formdata = new FormData()
        formdata.append('dokumen',isi.dokumen)
        formdata.append('isi',JSON.stringify(data))

        return new Promise((resolve,reject)=>{
            Axios.put(urlperizinan+'perizinan/verfizin/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                
                console.log('tes sukses');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.otorisasiizin]({},id){
        console.log(id);
        
        return new Promise((resolve,reject)=>{
            Axios.put(urlperizinan+'perizinan/otorisasi/'+id,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                console.log('berhasil otorisasi izin');
                
                }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.izin.otorisasiizinlist]({},isi){
        console.log(isi);
        
        //isi data "0,0,0"
        return new Promise((resolve,reject)=>{
            
            Axios.put(urlperizinan+'perizinan/otorisasi?listid='+isi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                console.log('berhasil');
                
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api izin

    // download dokumen surat deputi
    [Action.izin.downloaddokumenSD]({},isi){
        console.log(isi);
        
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/downloaddokumen/1/'+isi+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end download dokumen surat deputi

    // download dokumen surat pengajuan
    [Action.izin.downloaddokumenSP]({},isi){
        console.log(isi);
        
        return new Promise((resolve,reject)=>{
            Axios.get(urlperizinan+'perizinan/downloaddokumen/2/'+isi+'.',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end download dokumen surat pengajuan

    // edit verifikasidiklat
    [Action.diklat.verifikasiStatus]({},isi){
        console.log(isi);
        
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/verifikasi/'+isi.id+'/'+isi.verifikasi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                
                console.log('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end verifikasidiklat

    // otorisasi status
    [Action.diklat.otorisasiStatus]({},isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urldiklat+'diklat/otorisasi/'+isi.id+'/'+isi.otorisasi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                console.log('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    // end otorisasi status
    //api user
    [Action.user.tambahuser]({},isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            let data={
                "username":isi.username,
                "password":isi.password,
                "kementrianLembaga":isi.kld,
                "idInstansi":isi.idInstansi,
                "nama":isi.nama,
                "lemdik":isi.lemdik,
                "email":isi.email
            };
            Axios.post(urluser+'user/simpanuser/'+isi.role,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                console.log('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.userbyid]({},id){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftar/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambilnamauser]({},nama){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/ambilusernama?nama='+nama,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambilsemuauser](){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftar',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.userpenandatangan]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftarpenandatangan?kld='+isi.kld+'&lemdik='+isi.lemdik,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.buatpeserta]({},isi){
        //isi data {nip:"",instansi:""}
        return new Promise((resolve,reject)=>{
            Axios.post(urluser+'user/simpanpeserta/'+isi.nip+"/"+isi.instansi,'',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.buatuser](){
        return new Promise((resolve,reject)=>{
            Axios.post(urluser+'user/simpankld','',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.ambiluserbypembuat](){
        return new Promise((resolve,reject)=>{
            Axios.get(urluser+'user/daftaruser',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.gantiprofil]({},isi){
        //isi data {id:0,email:"",username:"",idinstansi:""}
        let data={
            "email":isi.email,
            "username":isi.username,
            "idInstansi":isi.idinstansi,
            "nama":isi.nama,
            "kementrianLembaga":isi.kementrianLembaga,
            "nik":isi.nik,
            "lemdik":isi.lemdik,
            "akronim":isi.akronim,
        }
        return new Promise((resolve,reject)=>{
            Axios.put(urluser+'user/gantiprofil/'+isi.id,data,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.gantipass]({},isi){
        //isi data {id:0,password:"",confirm:""}
        var formdata = new FormData()
        formdata.append('password',isi.password)
        formdata.append('confirm',isi.confirm)
        return new Promise((resolve,reject)=>{
            Axios.put(urluser+'user/gantipass/'+isi.id,formdata,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.user.hapususer]({},id){
        return new Promise((resolve,reject)=>{
            Axios.delete(urluser+'user/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api user
    
    //api sertifikat
     [Action.tambahdiklat]({commit,state},diklat){
        return new Promise ((resolve,reject)=>{

            let form ={
                "instansiPembina" : diklat.instansiPembina,
                "lembagaDiklat" : diklat.lembagaDiklat,
                "namaProgramDiklat" : diklat.namaProgramDiklat,
                "jenisProgramDiklat" : diklat.jenisProgramDiklat,
                "tahun" : diklat.tahun,
                "tempat": diklat.tempat,
                "angkatan" : diklat.angkatan,
                "penyelenggara" : diklat.penyelenggara,
                "tanggalPelaksanaan" : diklat.tanggalPelaksanaan,
                "tanggalSelesai" : diklat.tanggalSelesai,
                "kompetensi" : diklat.kompetensi,
                "persyaratanPeserta" : diklat.persyaratanPeserta,
                "saranaDanPrasarana" : diklat.saranaDanPrasarana,
                "evaluasiPeserta" : diklat.evaluasiPeserta,
                "evaluasiWidyaiswara" : diklat.evaluasiWidyaiswara,
                "evaluasiPenyelenggaraan" : diklat.evaluasiPenyelenggaraan,
                "sertifikasi" : diklat.sertifikasi
            }

            var formdiklat = new FormData()
            formdiklat.append('diklatJson',JSON.stringify(form))
            Axios.post('http://intranet.lan.go.id:8555/diklat/save',formdiklat,
            {
                headers:{
                    "Authorization":'Bearer '+window.$nuxt.$cookies.get('token'),
                    "Content-Type":'application/x-www-form-urlencoded'
                },
            }
            )
            .then(res=>{
                console.log(res)
                this.$router.push('/dashboard/diklat')
            })
            .catch(err=> {
                err=> console.warn(err)
                localStorage.removeItem('token')
                reject(err)
            })
        })
    },

    [Action.databkn]({},data){
        return new Promise((resolve,reject)=>{
            Axios.get('http://sirela.lan.go.id:8080/bkn/datapegawai/'+data.nip,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }) .then(res=>{resolve(res)}
            ).catch(err=>{reject(err)})
        })
    },

    [Action.daftardiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/lemdik',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.semuadiklat]({state,commit}){
        return new Promise((resolve,reject)=>{
            Axios.get('http://localhost:8091/diklat',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    // [Action.semuadiklat]({state,commit},status){
    //     return new Promise((resolve,reject)=>{
    //         Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
    //             headers:{
    //                  'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
    //             }
    //         }).then(res=>{resolve(res)}
    //         ).catch(err=>{reject(err)}
    //         )
    //     })
    // },

    [Action.lihatstatusdiklat]({state,commit},status){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/status/'+status,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ubahstatusdiklat]({state,commit},isi){
        var formdiklat = new FormData()

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/updatestatus/'+isi.diklat+'/'+isi.status,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                }
            }).then(res=>{
                resolve(res)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            ).catch(err=>{reject(err)
                this.$router.push('/dashboard/sertifikatDigital')
            }
            )
        })
    },

    [Action.uploadpeserta]({state,commit},diklat){
        console.log(window.$nuxt.$cookies.get('token'))
        console.log('isi: '+diklat.diklat)
        var formdiklat = new FormData()
        formdiklat.append('fileSurat',diklat.file)

        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/datapeserta/'+diklat.diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token'),
                     "Content-Type":'application/x-www-form-urlencoded'
                },
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.datapeserta]({state,commit},id){
        return new Promise((resolve,reject)=>{
            Axios.get('http://intranet.lan.go.id:8555/diklat/datapeserta/'+id,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdlemdik]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatanganlemdik/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttddeputi]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangandeputi/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.ttdkepala]({state,commit},diklat){
        var formdiklat = new FormData()
        return new Promise((resolve,reject)=>{
            Axios.post('http://intranet.lan.go.id:8555/diklat/tandatangankepala/'+diklat,formdiklat,{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end sertifikat

    //api sipka
    [Action.sipka.golpang](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'MGolPang',).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.sipka.provinsi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mprovinsi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.kabupaten](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mkabupaten').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.lemdik](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Mlemdik').then(res=>{resolve(res)
            console.log(res.data);
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.instansi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Minstansi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.prodik](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'MProgramDiklat').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.sipka.akreditasi](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Makreditasi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    //end api sipka

    //api master
    [Action.master.getUserByNamaLemdik]({},isi){
        console.log(isi)
        var formData = new FormData();
        formData.append('lemdik','re')
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/lemdikbyidSipka',{
                headers:{
                     'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
                }
            }).then(res=>{resolve(res)
                // window.location.reload(true);
                console.log('berhasil');
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.golpangMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/golpang',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postgolpangMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/golpang',{
                    "idSipka": isi.idSipka,
                    "pangkat": isi.pangkat,
                    "gol": isi.gol,
                    "gol_pangkat_tempat": isi.gol_pangkat_tempat,
                    "gol_pangkat_tingkat": isi.gol_pangkat_tingkat
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putgolpangMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/golpang/'+isi.id,{
                    "idSipka": isi.idSipka,
                    "pangkat": isi.pangkat,
                    "gol": isi.gol,
                    "gol_pangkat_tempat": isi.gol_pangkat_tempat,
                    "gol_pangkat_tingkat": isi.gol_pangkat_tingkat
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.provinsiMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/provinsi',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postprovinsiMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/provinsi',{
                "idSipka": isi.idSipka,
                "namaProvinsi": isi.namaProvinsi,
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putprovinsiMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/provinsi/'+isi.id,{
                "idSipka": isi.idSipka,
                "namaProvinsi": isi.namaProvinsi,
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.kabupatenMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/kabupaten',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postkabupatenMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/kabupaten',{
                "idSipka": isi.idSipka,
                "namaKabupaten": isi.namaKabupaten,
                "provinsi": isi.provinsi
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putkabupatenMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/kabupaten/'+isi.id,{
                "idSipka": isi.idSipka,
                "namaKabupaten": isi.namaKabupaten,
                "provinsi": isi.provinsi
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.lemdikMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/lemdik',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)
            console.log(res.data);
            }
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.addlemdikMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/lemdik',{
                "instansi": isi.instansi,
                "namaLemdik": isi.namaLemdik,
                "provinsi":isi.provinsi,
                "idSipka": isi.idSipka
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.updatelemdikMaster]({},isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/lemdik/'+isi.id,{
                "instansi": isi.instansi,
                "namaLemdik": isi.namaLemdik,
                "provinsi":isi.provinsi,
                "idSipka": isi.idSipka
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.postinstansiMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/instansi',
            {
                "idSipka": isi.idSipka,
                "namaInstansi": isi.namaInstansi,
                "kode_bkn": isi.kode_bkn,
            }
            ,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.putinstansiMaster]({},isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/instansi/'+isi.id,
            {
                "idSipka": isi.idSipka,
                "namaInstansi": isi.namaInstansi,
                "kode_bkn": isi.kode_bkn,
            }
            ,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.instansiMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/instansi',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.prodikMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlDev+'master/prodik',{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.addprodikMaster]({},isi){
        return new Promise((resolve,reject)=>{
            Axios.post(urlDev+'master/prodik',{
                "kuotaDiklat": isi.kuotaDiklat,
                "namaDiklat": isi.namaDiklat,
                "idSipka": isi.idSipka,
                "sektor": isi.sektor
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    [Action.master.updateprodikMaster]({},isi){
        console.log(isi);
        return new Promise((resolve,reject)=>{
            Axios.put(urlDev+'master/prodik/'+isi.id,{
                "kuotaDiklat": isi.kuotaDiklat,
                "namaDiklat": isi.namaDiklat,
                "idSipka": isi.idSipka,
                "sektor": isi.sektor
            },{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },

    [Action.master.akreditasiMaster](){
        return new Promise((resolve,reject)=>{
            Axios.get(urlmaster+'Makreditasi').then(res=>{resolve(res)}
            ).catch(err=>{reject(err)}
            )
        })
    },
    // Template
    [Action.master.getTemplate](){
        return new Promise((resolve, reject)=>{
            Axios.get(urlDev+"master/template",{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.master.postTemplate]({},isi){
        console.log(isi);
        let data ={
            "namaTemplate":isi.namaTemplate,
            "jenisPelatihan": isi.jenisPelatihan,
        }
        var formdata = new FormData()
        formdata.append('formatcert',isi.file[0])
        formdata.append('template',JSON.stringify(data))
        return new Promise((resolve, reject)=>{
            Axios.post(urlDev+"master/template",formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.master.putTemplate]({},isi){
        let data ={
            "namaTemplate":isi.namaTemplate,
            "jenisPelatihan": isi.jenisPelatihan,
        }
        var formdata = new FormData()
        formdata.append('formatcert',isi.file[0])
        formdata.append('template',JSON.stringify(data))
        return new Promise((resolve, reject)=>{
            Axios.put(urlDev+"master/template/"+isi.id,formdata,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    [Action.master.deleteTemplate]({},id){
        return new Promise((resolve, reject)=>{
            Axios.delete(urlDev+"master/template/"+id,{
                headers:{
                    'Authorization': 'Bearer '+window.$nuxt.$cookies.get('token')
               }
            }).then(res=>{
                resolve(res);
            }).catch(err=>{
                reject(err);
            })
        })
    },
    // end template
    //end api master

    //web socket
    [Action.send]() {
        if (this.stompClient && this.stompClient.connected) {
        //   console.log(send_message);
          this.stompClient.send("/app/diklat", "update berhasil", {});
        }
      },
      [Action.connect]({commit,state}) {
        //commit(`${[Mutation.connected]}`,true);
        console.log('koneksi '+state.connected);
        this.socket = new SockJS(urldiklat+"gs-guide-websocket");
        this.stompClient = Stomp.over(this.socket);
        this.stompClient.connect(
          {},
          frame => {
            commit(`${[Mutation.connected]}`,true);
            console.log('koneksi '+state.connected);
            console.log(frame);
            this.stompClient.subscribe("/front/alldiklat", tick => {
              console.log(tick);
              console.log(JSON.parse(tick.body));
              commit(`${[Mutation.received_messages]}`,JSON.parse(tick.body))

              //this.received_messages.push(JSON.parse(tick.body).konten);
            });
          },
          error => {
            console.log(error);
            commit(`${[Mutation.connected]}`,false);
          }
        );
      },
      [Action.disconnect]({commit}) {
        if (this.stompClient) {
          this.stompClient.disconnect();
        }
        commit(`${[Mutation.connected]}`,false);
      },
      [Action.tickleconn]() {
        this.$store.state.api.connected? this.disconnect() : this.connect();
      },
    //end  web socket


}

import {Mutation} from '../types';

export default {
  
    [Mutation.connected](state,connected){
        state.connected = connected
    },
    [Mutation.received_messages](state,listmsg){
        state.received_messages = listmsg
    },
    [Mutation.send_message](state,sendmsg){
        state.send_message=sendmsg
    },
    [Mutation.diklattsk](state,jenis){
        state.diklattsk=jenis
    },
    [Mutation.error](state,status){
        state.error=status
    }


}
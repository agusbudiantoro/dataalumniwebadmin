export const Action = {

    alumni: {
        editAlumni:'editAlumni',
        peserta: 'peserta',
        peserta_by_id: 'peserta_by_id',
        peserta_test: 'peserta_test',
        carialumni: 'carialumni',
        simpan_photo: 'simpan_photo',
        ambil_photo: 'ambil_photo',
        peserta_terbaik: 'peserta_terbaik',
        simpan_peserta_terbaik: 'simpan_peserta_terbaik',
        peserta_terbaik_by_diklat: 'peserta_terbaik_by_diklat',
        peserta_terbaik_get_all: 'peserta_terbaik_get_all',
        hapus_peserta_terbaik: 'hapus_peserta_terbaik',
        jadwal: 'jadwal',
        simpan_jadwal: 'simpan_jadwal',
        update_jadwal: 'update_jadwal',
        hapus_jadwal: 'hapus_jadwal',
        pengumuman: 'pengumuman',
        simpan_pengumuman: 'simpan_pengumuman',
        update_pengumuman: 'update_pengumuman',
        hapus_pengumuman: 'hapus_pengumuman',
        gambar_pengumuman: 'gambar_pengumuman',
        test_gambar_pengumuman: 'test_gambar_pengumuman',
        posting: 'posting',
        simpan_posting: 'simpan_posting',
        update_posting: 'update_posting',
        hapus_posting: 'hapus_posting',
        test_gambar_posting: 'test_gambar_posting',
        pendidikan_test: 'pendidikan_test',
        pendidikan: 'pendidikan',
        simpan_pendidikan: 'simpan_pendidikan',
        update_pendidikan: 'update_pendidikan',
        hapus_pendidikan: 'hapus_pendidikan',
        organisasi: 'organisasi',
        simpan_organisasi: 'simpan_organisasi',
        update_organisasi: 'update_organisasi',
        hapus_organisasi: 'hapus_organisasi',
        proper_test: 'proper_test',
        proper: 'proper',
        simpan_proper: 'simpan_proper',
        update_proper: 'update_proper',
        hapus_proper: 'hapus_proper',
        detail_proper: 'detail_proper',
        master_diklat: 'master_diklat',
        simpan_master_diklat: 'simpan_master_diklat',
        update_master_diklat: 'update_master_diklat',
        hapus_master_diklat: 'hapus_master_diklat',
        register: 'register',
        simpan_register: 'simpan_register',
        update_register: 'update_register',
        hapus_register: 'hapus_register',
        validasi_register: 'validasi_register'
    },

    // api
    tambahdiklat:'tambahdiklat',
    semuadiklat:'semuadiklat',
    uploadpeserta:'uploadpeserta',
    datapeserta:'datapeserta',
    ubahstatusdiklat:'ubahstatusdiklat',
    lihatstatusdiklat:'lihatstatusdikat',
    ttdlemdik:'ttdlemdik',
    ttddeputi:'ttddeputi',
    ttdkepala:'ttdkepala',
    daftardiklat:'daftardiklat',
    //end api

    //api diklat
    diklat:{
        putValidasi:'putValidasi',
        ubahtanggalttd:'ubahtanggalttd',
        ambildiklatbyuserid:'ambildiklatbyuserid',
        penandatangan:'penandatangan',
        downloaddoc:'downloaddoc',
        ambilsemuadiklat:'ambilsemuadiklat',
        ambildiklatbyuser:'ambildiklatbyuser',
        simpandiklat:'simpandiklat',
        kirimdiklat:'kirimdiklat',
        hitungpeserta:'hitungpeserta',
        //detail
        ambildetaildiklat:'ambildetaildiklat',
        simpandetail:'simpandetail',
        editdetail:'editdetail',
        deletedetail:'deletedetail',
        deletediklat:'deletediklat',
        //enddetail
        ambilangkatan:'ambilangkatan',
        simpanangkatan:'simpanangkatan',
        editangkatan:'editangkatan',
        delelteangkatan:'delelteangkatan',
        // verifikasistatus
        verifikasiStatus:'verifikasiStatus',
        // end verifikasi status
        // otorisasi
        otorisasiStatus:'otorisasiStatus',
        uploadkra:'uploadkra',
        // end otorisasi
        //angkatan
        //endangkatan
        postTemplateTeknis:'postTemplateTeknis'
    },
    //end api diklat

    //api kra
    kra:{
        tambah:'tambah',
        edit:'edit',
        ambil:'ambil',
        hapus:'hapus'
    },
    //end api kra
    //api cert
    cert:{
        cekganti:'cekganti',
        generate:'generate',
        ttdcert:'ttdcert',
        tandacert:'tandacert',
        ttdcertkalan:'ttdcertkalan',
        tandakalan:'tandakalan',
        ttdcertdeputi:'ttdcertdeputi',
        tandadeputi:'tandadeputi',
        gbtttd:'gbrttd',
        datagbr:'datagbr',
        cekFile:'cekFile'
    },
    //end api cert
    //api peserta
    peserta:{
        ambilpesertabyIdPeserta:'ambilpesertabyIdPeserta',
        ambilpesertabyrole:'ambilpesertabyrole',
        revisiSttp:'revisiSttp',
        ambilpesertatanpaid:'ambilpesertatanpaid',
        ambilpesertabyid:'ambilpesertabyid',
        tambahkra:'tambahkra',
        tambahnilai:'tambahnilai',
        updatePeserta:'updatePeserta',
        ambilbkn:'ambilbkn',
        simpanpesertakelompok:'simpanpesertakelompok',
        ambilpesertabydiklat:'ambilpesertabydiklat',
        ambilpesertabyangkatan:'ambilpesertabyangkatan',
        simpanpeserta:'simpanpeserta',
        editpeserta:'editpeserta',
        editfotopeserta:'editfotopeserta',
        getfotopeserta:'getfotopeserta',
        deletepeserta:'deletepeserta',
        pindahangkatan:'pindahangkatan',
        pindahdiklat:'pindahdiklat',
        downloadsampel:'downloadsampel',
    },
    //end api peserta

    //api izin
    izin:{
        ambilizin:'ambilizin',
        ambilizinbyuser:'ambilizinbyuser',
        editizin:'editizin',
        deleteizin:'deleteizin',
        ajukanizin:'ajukanizin',
        verifikasiizin:'verifikasiizin',
        otorisasiizin:'otorisasiizin',
        otorisasiizinlist:'otorisasiizinlist',
        downloaddokumenSD:'downloaddokumenSD', //surat deputi
        downloaddokumenSP:'downloaddokumenSP' //surat pengajuan
    },
    //end izin

    //api user
    user:{
        userpenandatangan:'userpenandatangan',
        userbyid:'userbyid',
        tambahuser:'tambahuser',
        ambilsemuauser:'ambilsemuauser',
        buatuser:'buatuser',
        ambiluserbypembuat:'ambiluserbypembuat',
        buatpeserta:'buatpeserta',
        gantiprofil:'gantiprofil',
        gantipass:'gantipass',
        hapususer:'hapususer',
        ambilnamauser:'ambilnamauser',
    },
    //end user

    //api bkn
    databkn:'databkn',
    //api bkn

    //sipka
    sipka:{
        provinsi:'provinsi',
        kabupaten:'kabupaten',
        lemdik:'lemdik',
        prodik:'prodik',
        instansi:'instansi',
        akreditasi:'akreditasi',
        golpang:'golpang1',
    },
    //endsipka
    //master
    master:{
        getUserByNamaLemdik:'getUserByNamaLemdik',
        provinsiMaster:'provinsiMaster',
        postprovinsiMaster:'postprovinsiMaster',
        putprovinsiMaster:'putprovinsiMaster',
        kabupatenMaster:'kabupatenMaster',
        postkabupatenMaster:'postkabupatenMaster',
        putkabupatenMaster:'putkabupatenMaster',
        lemdikMaster:'lemdikMaster',
        addlemdikMaster:'addlemdikMaster',
        updatelemdikMaster:'updatelemdikMaster',

        golpangMaster:'golpangMaster',
        postgolpangMaster:'postgolpangMaster',
        putgolpangMaster:'putgolpangMaster',

        prodikMaster:'prodikMaster',
        addprodikMaster:'addprodikMaster',
        updateprodikMaster:'updateprodikMaster',

        instansiMaster:'instansiMaster',
        postinstansiMaster:'postinstansiMaster',
        putinstansiMaster:'putinstansiMaster',
        akreditasiMaster:'akreditasiMaster',
        golpangMaster:'golpangMaster',
        getTemplate:'getTemplate',
        postTemplate:'postTemplate',
        putTemplate:'putTemplate',
        deleteTemplate:'deleteTemplate'
    },
    //end master

    //websocket
    send:'send',
    connect:'connect',
    disconnect:'disconnect',
    tickleconn:'tickleconn',
    //endwebsocket

};

export const Mutation ={
    diklat:'diklat',
    received_messages: 'received_messages',
    send_message: 'send_message',
    connected: 'connected',
    diklattsk:'diklattsk',
    error:'error',
}



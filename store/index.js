import Vuex from 'vuex';
import Vue from 'vue'
import auth from './auth';
import api from './api';

Vue.use(Vuex)
const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    modules: {
      auth: auth,
      api: api
    }
  });
};

export default createStore
